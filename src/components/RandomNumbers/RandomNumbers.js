import React from 'react';
import './RandomNumbers.css';

const RandomNumbers = props => {
    return (
        <div className="number">{props.number}</div>
    );
};

export default RandomNumbers;