import {Component} from "react";
import './App.css';
import RandomNumbers from "./components/RandomNumbers/RandomNumbers";

class App extends Component {
    state = {
        number: []
    };

    changeNumber = () => {
        const newList = [];

        while (true) {
            if (newList.length === 5) {
                break;
            }

            const number = Math.floor(Math.random() * (36 - 5 + 1) + 5);

            if (!newList.includes(number)) {
                newList.push(number);
            }
        }

        this.setState({
            number: newList
        });
    };

    render() {
        const numbersDivs = this.state.number.map(number => (
            <RandomNumbers number={number}/>
        ));

        return (
            <div className="container">
                <div>
                    <button className="btn" onClick={this.changeNumber}>
                        New numbers
                    </button>
                </div>
                <div>
                    {numbersDivs}
                </div>
            </div>
        );
    }
}

export default App;
